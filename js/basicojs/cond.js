/*Manejo de condicionales */

function suma(x,y){
  return x+y;
}

function ispar(x){
  if(x % 2 === 0){
    return true;
  }else{
    return false;
  }
}

function noespar(x){
  if(x % 2 === 0){
    return false;
  }else{
    return true;
  }
}

function isPrimo(x){
  var raiz = Math.sqrt(x)
  raiz = Math.ceil(raiz)
  console.log(raiz);
  for(var i = 2; i < raiz ;i++){

    if( x % i === 0){
      return false;
    }
  }

  return true;
}

if (require.main === module) {

    var x = ispar(suma(2,2));
    var y = noespar(suma(2,2));

    console.log(x);
    console.log(y);
    console.log(isPrimo(3));
    console.log(isPrimo(17));
    console.log(isPrimo(33));
    console.log(isPrimo(100));
    console.log(isPrimo(101));

}
