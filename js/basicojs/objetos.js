
function persona(firstName, lastName, age, eyeColor) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.eyeColor = eyeColor;
    this.changeName = function (name) {
        this.lastName = name;
    };
}





if (require.main === module) {
  var car = {type:"Fiat", model:"500", color:"white"};
  var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue",
                fullName: function(){
                  return this.firstName + " " + this.lastName;
                }};
  var persona = new persona("felipe", "jaja",50, "negro");
  persona.changeName("Marin");
  console.log(persona);
  console.log(car.color);
  console.log(car["color"]);
  console.log(person.fullName());
}
